using System;
using SiCProject.Models;
using System.Collections.Generic;

namespace SiCProject.DTOs
{
    public class AcabamentoDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public float Incremento { get; set; }
        public int MaterialId { get; set; }

        public AcabamentoDTO(int id, string nomemat, float incremento, int materialId)
        {
            this.ID = id;
            this.Nome = nomemat;
            this.Incremento = Incremento;
            this.MaterialId = materialId;
        }

    }
}