using System;
using SiCProject.Models;
using System.Collections.Generic;

namespace SiCProject.DTOs
{
    public class MaterialDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public List<String> Acabamentos { get; set; }
        public float Preco { get; set; }

        public MaterialDTO(int id, string nomemat, List<String> acabs, float Preco)
        {
            this.ID = id;
            this.Nome = nomemat;
            this.Acabamentos = acabs;
            this.Preco = Preco;
        }

    }
}