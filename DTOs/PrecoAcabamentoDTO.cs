using System;
using SiCProject.Models;
using System.Collections.Generic;

namespace SiCProject.DTOs
{
    public class PrecoAcabamentoDTO
    {
        public int ID { get; set; }
        public float preco { get; set; }
        public DateTime data { get; set; }
        public int AcabamentolId { get; set; }

        public PrecoAcabamentoDTO(int id, float preco, DateTime data, int acabamentoId)
        {
            this.ID = id;
            this.preco = preco;
            this.data = data;
            this.AcabamentolId = acabamentoId;
        }

    }
}