using System;
using SiCProject.Models;
using SiCProject.Models.Restricoes;

namespace SiCProject.Services
{
    public static class Serviço
    {
        
        public static bool restricaoCaber(double ap, double af, double lp, double lf, double pp, double pf){

            if(ap < af){
                return false;
            }
            if(lp < lf){
                return false;
            }
            if(pp < pf){
                return false;
            }

            return true;
        }

        public static double getVolume(double alt, double larg, double prof){
            return alt * larg * prof;
        }

        public static bool restricaoOcupacao(RestricaoOcupacao restricao, double Vpai, double Vfilho){

            String limites = Vpai +":"+Vfilho;

            return restricao.checkRestricao(limites);
        }

        public static bool restricaoMaterial(RestricaoMaterial restricao, String matPai, String matFilho){

            String materiais = matPai+":"+matFilho;

            return restricao.checkRestricao(materiais);
        }
    }
}