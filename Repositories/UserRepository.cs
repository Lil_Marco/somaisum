using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject.Repositories
{
    public class UserRepository : Repository<User>
    {

        private readonly SiCContext _context;

        public UserRepository(SiCContext context)
        {
            _context = context;
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<User> GetAll()
        {

            List<User> cats = new List<User>();
            foreach (User u in _context.Users)
            {
                cats.Add(u);
            }
            return cats;

        }

        public User GetByID(int id)
        {
            throw new NotImplementedException();
        }

        public User Post(User obj)
        {
            throw new NotImplementedException();
        }

        public bool Put(User obj)
        {
            throw new NotImplementedException();
        }
    }

}