﻿using SiCProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.Repositories
{
    interface IColecaoRepository : Repository<Colecao>
    {
    }
}
