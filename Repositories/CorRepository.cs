using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;


namespace SiCProject.Repositories
{
    public class CorRepository : CorRepositoryInterface
    {

        private readonly SiCContext _context;

        public CorRepository(SiCContext context)
        {
            _context = context;
        }

        public CorDTO getInfoCorDTO(Cor c)
        {

            return new CorDTO(c.ID, c.Nome, c.rgbRed, c.rgbAzul, c.rgbVerde);
        }

        public List<Cor> GetAll()
        {
            List<Cor> cor = new List<Cor>();
            foreach (Cor corr in _context.Cores)
            {
                cor.Add(corr);
            }
            return cor;
        }

        public Cor GetByID(int id)
        {
            Cor cor = _context.Cores.Find(id);

            return cor;
        }

        public List<Cor> GetByNome(string nome)
        {
            IQueryable<Cor> cor = _context.Cores.Where(c => c.Nome.Equals(nome));
            List<Cor> lista = cor.ToList();

            return lista;
        }

        public Cor Post(Cor obj)
        {
            _context.Cores.Add(obj);

            _context.SaveChangesAsync();

            return obj;
        }

        public bool Put(Cor obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var cor = _context.Cores.Find(id);

            if (cor == null)
            {
                return false;
            }

            _context.Cores.Remove(cor);
            _context.SaveChangesAsync();

            return true;
        }

        private bool CorExists(int id)
        {
            return _context.Cores.Any(e => e.ID == id);
        }
    }
}