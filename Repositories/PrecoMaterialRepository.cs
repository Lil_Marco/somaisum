using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;
using SiCProject.Repositories;


namespace SiCProject.Repositories
{
    public class PrecoMaterialRepository : PrecoMaterialRepositoryInterface
    {

        private readonly SiCContext _context;
        private MaterialRepository mr;

        public PrecoMaterialRepository(SiCContext context)
        {
            _context = context;
        }

        public PrecoMaterialDTO getInfoPrecoMaterialDTO(PrecoMaterial c)
        {
            return new PrecoMaterialDTO(c.ID, c.preco, c.data, c.MaterialId);
        }

        public List<PrecoMaterial> GetAll()
        {
            List<PrecoMaterial> cor = new List<PrecoMaterial>();
            foreach (PrecoMaterial corr in _context.PrecoMateriais)
            {
                cor.Add(corr);
            }
            return cor;
        }

        public PrecoMaterial GetByID(int id)
        {
            PrecoMaterial cor = _context.PrecoMateriais.Find(id);

            return cor;
        }

        public PrecoMaterial Post(PrecoMaterial obj)
        {
            _context.PrecoMateriais.Add(obj);


            Material m = new Material();
            m = _context.Materiais.Find(obj.MaterialId);
            List<PrecoMaterial> pMs = new List<PrecoMaterial>();
            pMs = this.GetAll();
            DateTime date = DateTime.Now;
            int count = 0;
            float preco = 0;
            DateTime dataPreco = new DateTime();

            if (pMs.Any())
            {
                foreach (PrecoMaterial pM in pMs)
                {
                    if (pM.MaterialId == m.ID)
                    {
                        if (DateTime.Compare(date, pM.data) > 0)
                        {
                            if (count == 0)
                            {
                                preco = pM.preco;
                                dataPreco = pM.data;
                            }
                            else
                            {
                                if (DateTime.Compare(pM.data, dataPreco) > 0)
                                {
                                    preco = pM.preco;
                                    dataPreco = pM.data;
                                }
                            }
                        }

                    }
                }
                m.Preco = preco;
            }
            else
            {
                m.Preco = obj.preco;
            }




            _context.Entry(m).State = EntityState.Modified;

            // _context.SaveChangesAsync();

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return null;
            }

            return obj;
        }

        public bool Put(PrecoMaterial obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var cor = _context.PrecoMateriais.Find(id);

            if (cor == null)
            {
                return false;
            }

            _context.PrecoMateriais.Remove(cor);
            _context.SaveChangesAsync();

            return true;
        }

        private bool PrecoMaterialExists(int id)
        {
            return _context.PrecoMateriais.Any(e => e.ID == id);
        }
    }
}