using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.DTOs;
using SiCProject.Repositories;


namespace SiCProject.Repositories
{
    public class PrecoAcabamentoRepository : PrecoAcabamentoRepositoryInterface
    {

        private readonly SiCContext _context;
        private AcabamentoRepository mr;

        public PrecoAcabamentoRepository(SiCContext context)
        {
            _context = context;
        }

        public PrecoAcabamentoDTO getInfoPrecoAcabamentoDTO(PrecoAcabamento c)
        {

            return new PrecoAcabamentoDTO(c.ID, c.preco, c.data, c.AcabamentoId);
        }

        public List<PrecoAcabamento> GetAll()
        {
            List<PrecoAcabamento> cor = new List<PrecoAcabamento>();
            foreach (PrecoAcabamento corr in _context.PrecoAcabamentos)
            {
                cor.Add(corr);
            }
            return cor;
        }

        public PrecoAcabamento GetByID(int id)
        {
            PrecoAcabamento cor = _context.PrecoAcabamentos.Find(id);

            return cor;
        }

        public PrecoAcabamento Post(PrecoAcabamento obj)
        {
            _context.PrecoAcabamentos.Add(obj);

            Acabamento m = new Acabamento();
            m = _context.Acabamentos.Find(obj.AcabamentoId);
            List<PrecoAcabamento> pMs = new List<PrecoAcabamento>();
            pMs = this.GetAll();
            DateTime date = DateTime.Now;
            int count = 0;
            float preco = 0;
            DateTime dataPreco = new DateTime();

            if (pMs.Any())
            {
                foreach (PrecoAcabamento pM in pMs)
                {
                    if (pM.AcabamentoId == m.ID)
                    {
                        if (DateTime.Compare(date, pM.data) > 0)
                        {
                            if (count == 0)
                            {
                                preco = pM.preco;
                                dataPreco = pM.data;
                            }
                            else
                            {
                                if (DateTime.Compare(pM.data, dataPreco) > 0)
                                {
                                    preco = pM.preco;
                                    dataPreco = pM.data;
                                }
                            }
                        }

                    }
                }
                m.Incremento = preco;
            }
            else
            {
                m.Incremento = obj.preco;
            }

            _context.Entry(m).State = EntityState.Modified;
            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return null;
            }

            return obj;
        }

        public bool Put(PrecoAcabamento obj)
        {
            _context.Entry(obj).State = EntityState.Modified;

            try
            {
                _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
            return true;
        }

        public bool Delete(int id)
        {
            var cor = _context.PrecoAcabamentos.Find(id);

            if (cor == null)
            {
                return false;
            }

            _context.PrecoAcabamentos.Remove(cor);
            _context.SaveChangesAsync();

            return true;
        }

        private bool PrecoAcabamentoExists(int id)
        {
            return _context.PrecoAcabamentos.Any(e => e.ID == id);
        }
    }
}