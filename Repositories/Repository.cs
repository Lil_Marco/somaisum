using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

public interface Repository<T>
{
    List<T> GetAll();
    T GetByID(int id);
    bool Put(T obj);
    T Post(T obj);
    bool Delete(int id);
}