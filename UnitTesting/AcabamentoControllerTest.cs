using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Controllers;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class AcabamentoControllerTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            Acabamento acab1 = new Acabamento{
                Nome = "polido"
            };

            Acabamento acab2 = new Acabamento{
                Nome = "envernizado"
            };

            _context.Acabamentos.Add(acab1);
            _context.Acabamentos.Add(acab2);

            _context.SaveChanges();
        }

        [Fact]
        public void Test_GetAll(){
            using(var _context = InitDBSet("Test_GetAll_C")){

                DB(_context);
                var _mController = new AcabamentoController(_context);
                long tamanho_esperado = 2;

                int resultado = 0;

                using(IEnumerator<Acabamento> CharEnumerator = _mController.GetAcabamentos().GetEnumerator()){
                    while(CharEnumerator.MoveNext())
                        resultado++;
                }

                Assert.Equal(tamanho_esperado, resultado);
            }
        }

    [Fact]
        public async void Test_PutAcabamento()
        {
            using (var _context = InitDBSet("Test_PutAcabamento_M"))
            {
                var _mController = new AcabamentoController(_context);
                Assert.Equal(0, await _context.Acabamentos.CountAsync());

                Acabamento A = new Acabamento
                {
                    Nome = "Envernizado_test"
                };

                await _context.Acabamentos.AddAsync(A);
                await _context.SaveChangesAsync();

                Assert.Equal(1, await _context.Acabamentos.CountAsync());

                int Aid = 1;
                A.Nome = "Test";

                var acab = await _mController.PutAcabamento(Aid, A);

            }
        }
        [Fact]
        public async void Test_PostAcabamento()
        {
            using (var _context = InitDBSet("Test_PostAcabamento"))
            {
                DB(_context);
                var _mController = new AcabamentoController(_context);

                Acabamento A = new Acabamento();

                var acab = await _mController.PostAcabamento(A);
                Assert.IsType<CreatedAtActionResult>(acab);

            }
        }

        [Fact]
        public async void Test_DeleteAcabamento()
        {
            using (var _context = InitDBSet("Test_DeleteAcabamento_M"))
            {
                Acabamento A = new Acabamento
                {
                    Nome = "Envernizado"
                };

                _context.Acabamentos.Add(A);

                _context.SaveChanges();

                var _mController = new AcabamentoController(_context);

                var acab = await _mController.DeleteAcabamento(A.ID);
                Assert.IsType<OkObjectResult>(acab);
            }
        }

        [Fact]
        public async void Test_GetMaterialAcabamento()
        {
            using (var _context = InitDBSet("Test_GetMaterialAcabamento"))
            {
                DB(_context);
                var _mController = new AcabamentoController(_context);

                Acabamento A = await _context.Acabamentos.FirstAsync();

                var acab = await _mController.GetAcabamento(A.ID);
                Assert.IsType<OkObjectResult>(acab);

                acab = await _mController.GetAcabamento((int)5000);
                Assert.IsType<NotFoundResult>(acab);
            }
        }

    }
}