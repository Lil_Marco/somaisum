/*using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Controllers;
using Xunit;

namespace SiCProject.UnitTesting
{
    public class MaterialControllerTest
    {
        private SiCContext InitDBSet(String DbName){
            SiCContext _context;

            var option = new DbContextOptionsBuilder<SiCContext>()
                .UseInMemoryDatabase(DbName)
                .Options;

            _context = new SiCContext(option);
            _context.Database.EnsureCreated();
            _context.Database.EnsureDeleted();

            return _context;
        }

        private void DB(SiCContext _context){

            Material mat1 = new Material{
                Nome = "madeira",
                Acabamento=null
            };

            Material mat2 = new Material{
                Nome = "ferro",
                Acabamento=null
            };

            _context.Materiais.Add(mat1);
            _context.Materiais.Add(mat2);

            _context.SaveChanges();
        }

        [Fact]
        public void Test_GetAll(){
            using(var _context = InitDBSet("Test_GetAll_C")){

                DB(_context);
                var _mController = new MaterialController(_context);
                long tamanho_esperado = 2;

                int resultado = 0;
                IEnumerable<Material> lol = _context.Materiais;
                using(IEnumerator<Material> CharEnumerator = lol.GetEnumerator()){
                    while(CharEnumerator.MoveNext())
                        resultado++;
                }

                Assert.Equal(tamanho_esperado, resultado);
            }
        }

    [Fact]
        public async void Test_PutMaterial()
        {
            using (var _context = InitDBSet("Test_PutMaterial_M"))
            {
                var _mController = new MaterialController(_context);
                Assert.Equal(0, await _context.Materiais.CountAsync());

                Material m = new Material
                {
                    Nome = "madeira_test",
                    Acabamento = null
                };

                await _context.Materiais.AddAsync(m);
                await _context.SaveChangesAsync();

                Assert.Equal(1, await _context.Materiais.CountAsync());

                int mid = 1;
                m.Nome = "Test";

                var acab = await _mController.PutMaterial(mid, m);

            }
        }
        [Fact]
        public async void Test_PostMaterial()
        {
            using (var _context = InitDBSet("Test_PostMaterial"))
            {
                DB(_context);
                var _mController = new MaterialController(_context);

                Material m = new Material();

                var mat = await _mController.PostMaterial(m);
                Assert.IsType<CreatedAtActionResult>(mat);

            }
        }

        [Fact]
        public async void Test_DeleteMaterial()
        {
            using (var _context = InitDBSet("Test_DeleteMaterial_M"))
            {
                Material m = new Material
                {
                    Nome = "madeira",
                    Acabamento = null
                };

                _context.Materiais.Add(m);

                _context.SaveChanges();

                var _mController = new MaterialController(_context);

                var mat = await _mController.DeleteMaterial(m.ID);
                Assert.IsType<OkObjectResult>(mat);
            }
        }

        [Fact]
        public async void Test_GetMaterial()
        {
            using (var _context = InitDBSet("Test_GetMaterial"))
            {
                DB(_context);
                var _mController = new MaterialController(_context);

                Material m = await _context.Materiais.FirstAsync();

                var mat = await _mController.GetMaterial(m.ID);
                Assert.IsType<OkObjectResult>(mat);
            }
        }
    }
}*/