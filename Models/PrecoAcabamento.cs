using System;

namespace SiCProject.Models
{
    public class PrecoAcabamento
    {
        public int ID { get; set; }

        public float preco { get; set; }

        public DateTime data { get; set; }

        public int AcabamentoId { get; set; }

    }
}