using System;
using System.Collections.Generic;

namespace SiCProject.Models
{
    public class Produto
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public float Preço { get; set; }

        public int CategoriaID { get; set; }
        public virtual Categoria Categoria { get; set; }

        public virtual List<Material> Materiais { get; set; }
        public virtual List<Cor> Cores { get; set; }

        public int DimensaoID { get; set; }
        public virtual Dimensao Dimensao { get; set; }
    }
}