﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SiCProject.Models
{
    public class LinhaEstetica
    {

        public int ID { get; set; }
        public string Nome { get; set; }
       

        public const string CLIENTE = "CLIENTE";
        public const string GESTOR_CATALOGOS = "GESTOR_CATALOGOS";
        public const string SYS_ADMIN = "SYS_ADMIN";

    }
}
