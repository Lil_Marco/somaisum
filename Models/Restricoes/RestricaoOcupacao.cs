using System;

namespace SiCProject.Models.Restricoes
{
    public class RestricaoOcupacao  : Restricao
    {
        public double limMin {get; set;}

        public double limMax {get; set;}

        public override bool checkRestricao(String info){

            string[] volumes = info.Split(':');
            double Vpai = Double.Parse(volumes[0]);
            double Vfilho = Double.Parse(volumes[1]);

            if(Vpai * (this.limMin*0.01) > Vfilho){
                return false;
            }

            if(Vpai * (this.limMax*0.01) < Vfilho){
                return false;
            }

            return true;
        }
    }
}