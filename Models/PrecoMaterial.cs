using System;

namespace SiCProject.Models
{
    public class PrecoMaterial
    {
        public int ID { get; set; }

        public float preco { get; set; }

        public DateTime data { get; set; }

        public int MaterialId { get; set; }

    }
}