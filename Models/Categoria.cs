using System;
using System.Collections.Generic;

namespace SiCProject.Models{


    public class Categoria{

        public int ID {get; set;}
        public string nome {get; set;}
        public List<Categoria> SubCategorias{get;set;}
    }
}