using System;
using System.Collections.Generic;

namespace SiCProject.Models
{
    public class Composicao
    {
        public int ID {get; set;}

        public bool Obrigatoria {get; set;}

        public int ProdutoPaiID {get; set;}

        public int ProdutoFilhoID {get; set;}
    }
}