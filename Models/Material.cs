using System;
using System.Collections.Generic;

namespace SiCProject.Models
{
    public class Material
    {
        public int ID { get; set; }

        public string Nome { get; set; }

        public virtual List<Acabamento> Acabamentos { get; set; }

        public float Preco { get; set; }

    }
}