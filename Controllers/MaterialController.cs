using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Repositories;
using SiCProject.DTOs;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaterialController : ControllerBase
    {
        private readonly SiCContext _context;

        private MaterialRepository _repository;

#pragma warning disable 1998
        public MaterialController(SiCContext context)
        {
            _context = context;
            _repository = new MaterialRepository(_context);
        }



        // GET: api/Material
        [HttpGet]
        public async Task<IActionResult> GetMateriais()
        {
            List<Material> lista = _repository.GetAll();

            List<MaterialDTO> dtos = new List<MaterialDTO>();

            foreach (Material mat in lista)
            {
                dtos.Add(_repository.getInfoMaterialDTO(mat));
            }

            return Ok(dtos);
        }

        // GET: api/Material/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMaterialByID([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Material material = _repository.GetByID(id);

            if (material == null)
            {
                return NotFound();
            }

            return Ok(_repository.getInfoMaterialDTO(material));
        }

        [HttpGet("nome={nome}")]
        public async Task<IActionResult> GetMaterialByName([FromRoute] string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var produto = await _context.Produtos.FindAsync(id);
            List<Material> lista = _repository.GetByNome(nome);

            if (lista == null)
            {
                return NotFound();
            }

            List<MaterialDTO> dtos = new List<MaterialDTO>();
            foreach (Material m in lista)
            {
                dtos.Add(_repository.getInfoMaterialDTO(m));
            }
            return Ok(dtos);

        }

        // PUT: api/Material/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMaterial([FromRoute] int id, [FromBody] Material material)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != material.ID)
            {
                return BadRequest();
            }

            if (_repository.Put(material))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Material
        [HttpPost]
        public async Task<IActionResult> PostMaterial([FromBody] Material material)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Material mat = _repository.Post(material);
            if (mat != null)
            {
                return Ok(_repository.getInfoMaterialDTO(mat));
            }
            return BadRequest("Nome existente! Insira outro");
        }

        // DELETE: api/Material/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMaterial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_repository.Delete(id))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        private bool MaterialExists(int id)
        {
            return _context.Materiais.Any(e => e.ID == id);
        }
    }
}