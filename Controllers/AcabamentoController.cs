using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Repositories;
using SiCProject.Models;
using SiCProject.DTOs;

namespace SiCProject.Controllers
{
    //  [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class AcabamentoController : ControllerBase
    {
        private readonly SiCContext _context;
        private AcabamentoRepository _repository;

        public AcabamentoController(SiCContext context)
        {
            _context = context;
            _repository = new AcabamentoRepository(_context);
        }

        // GET: api/Acabamento
        [HttpGet]
        public IEnumerable<Acabamento> GetAcabamentos()
        {
            return _context.Acabamentos;
        }

        // GET: api/Acabamento/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var acabamento = await _context.Acabamentos.FindAsync(id);

            if (acabamento == null)
            {
                return NotFound();
            }

            return Ok(acabamento);
        }

        // PUT: api/Acabamento/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAcabamento([FromRoute] int id, [FromBody] Acabamento acabamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != acabamento.ID)
            {
                return BadRequest();
            }

            _context.Entry(acabamento).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AcabamentoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Acabamento
        [HttpPost]
        public async Task<IActionResult> PostAcabamento([FromBody] Acabamento acabamento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Acabamento acab = _repository.Post(acabamento);

            if (acab != null)
            {
                return Ok(_repository.getInfoAcabamentoDTO(acab));
            }
            return BadRequest("Nome existente! Insira outro");
        }

        // DELETE: api/Acabamento/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAcabamento([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var acabamento = await _context.Acabamentos.FindAsync(id);
            if (acabamento == null)
            {
                return NotFound();
            }

            _context.Acabamentos.Remove(acabamento);
            await _context.SaveChangesAsync();

            return Ok(acabamento);
        }

        private bool AcabamentoExists(int id)
        {
            return _context.Acabamentos.Any(e => e.ID == id);
        }


        [HttpGet("nome={nome}")]
        public async Task<IActionResult> GetAcabamentoByName([FromRoute] string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var produto = await _context.Produtos.FindAsync(id);
            List<Acabamento> lista = _repository.GetByNome(nome);

            if (lista == null)
            {
                return NotFound();
            }

            List<AcabamentoDTO> dtos = new List<AcabamentoDTO>();
            foreach (Acabamento a in lista)
            {
                dtos.Add(_repository.getInfoAcabamentoDTO(a));
            }
            return Ok(dtos);

        }
    }
}