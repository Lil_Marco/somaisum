using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Repositories;
using SiCProject.DTOs;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrecoMaterialController : ControllerBase
    {
        private readonly SiCContext _context;
        private PrecoMaterialRepository _repository;

        public PrecoMaterialController(SiCContext context)
        {
            _context = context;
            _repository = new PrecoMaterialRepository(_context);
        }

        // GET: api/PrecoMaterial
        [HttpGet]
        public async Task<IActionResult> GetPrecoMateriais()
        {
            List<PrecoMaterial> lista = _repository.GetAll();
            List<PrecoMaterialDTO> dtos = new List<PrecoMaterialDTO>();

            foreach (PrecoMaterial preco in lista)
            {
                dtos.Add(_repository.getInfoPrecoMaterialDTO(preco));
            }

            return Ok(dtos);
        }

        // GET: api/PrecoMaterial/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPrecoMaterial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PrecoMaterial precoMaterial = _repository.GetByID(id);

            if (precoMaterial == null)
            {
                return NotFound();
            }

            return Ok(_repository.getInfoPrecoMaterialDTO(precoMaterial));
        }

        // PUT: api/PrecoMaterial/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPrecoMaterial([FromRoute] int id, [FromBody] PrecoMaterial precoMaterial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != precoMaterial.ID)
            {
                return BadRequest();
            }

            if (_repository.Put(precoMaterial))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/PrecoMaterial
        [HttpPost]
        public async Task<IActionResult> PostPrecoMaterial([FromBody] PrecoMaterial precoMaterial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            PrecoMaterial preco = _repository.Post(precoMaterial);


            return Ok(_repository.getInfoPrecoMaterialDTO(preco));
        }

        // DELETE: api/PrecoMaterial/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePrecoMaterial([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_repository.Delete(id))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        private bool PrecoMaterialExists(int id)
        {
            return _context.PrecoMateriais.Any(e => e.ID == id);
        }
    }
}