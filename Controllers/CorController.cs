using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Repositories;
using SiCProject.DTOs;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CorController : ControllerBase
    {
        private readonly SiCContext _context;
        private CorRepository _repository;
        public CorController(SiCContext context)
        {
            _context = context;
            _repository = new CorRepository(_context);
        }
        
        // GET: api/Cor
        [HttpGet]
        public async Task<IActionResult> GetCores()
        {
            List<Cor> lista = _repository.GetAll();

            List<CorDTO> dtos = new List<CorDTO>();

            foreach (Cor cores in lista)
            {
                dtos.Add(_repository.getInfoCorDTO(cores));
            }

            return Ok(dtos);
        }

        // GET: api/Cor/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCorByID([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Cor cor = _repository.GetByID(id);

            if (cor == null)
            {
                return NotFound();
            }

            return Ok(_repository.getInfoCorDTO(cor));
        }
        [HttpGet("nome={nome}")]
        public async Task<IActionResult> GetMaterialByName([FromRoute] string nome)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //var produto = await _context.Produtos.FindAsync(id);
            List<Cor> lista = _repository.GetByNome(nome);

            if (lista == null)
            {
                return NotFound();
            }

            List<CorDTO> dtos = new List<CorDTO>();
            foreach (Cor m in lista)
            {
                dtos.Add(_repository.getInfoCorDTO(m));
            }
            if (dtos.Count != 0)
            {
                return Ok(dtos);
            }
            else
            {
                return NotFound();
            }

        }

        // PUT: api/Cor/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCor([FromRoute] int id, [FromBody] Cor cor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != cor.ID)
            {
                return BadRequest();
            }

            if (_repository.Put(cor))
            {
                return NoContent();
            }
            else
            {
                return NotFound();
            }
        }

        // POST: api/Cor
        [HttpPost]
        public async Task<IActionResult> PostCor([FromBody] Cor cor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Cor cores = _repository.Post(cor);


            return Ok(_repository.getInfoCorDTO(cores));
        }

        // DELETE: api/Cor/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCor([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_repository.Delete(id))
            {

                return NoContent();
            }
            else
            {
                return NotFound();
            }

        }

        private bool CorExists(int id)
        {
            return _context.Cores.Any(e => e.ID == id);
        }
    }
}