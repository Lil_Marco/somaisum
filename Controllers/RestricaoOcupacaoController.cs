using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;
using SiCProject.Models.Restricoes;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestricaoOcupacaoController : ControllerBase
    {
        private readonly SiCContext _context;

        public RestricaoOcupacaoController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/RestricaoOcupacao
        [HttpGet]
        public IEnumerable<RestricaoOcupacao> GetRestricaoOcupacao()
        {
            return _context.RestricoesOcupacao;
        }

        // GET: api/RestricaoOcupacao/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetRestricaoOcupacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricaoOcupacao = await _context.RestricoesOcupacao.FindAsync(id);

            if (restricaoOcupacao == null)
            {
                return NotFound();
            }

            return Ok(restricaoOcupacao);
        }

        // PUT: api/RestricaoOcupacao/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRestricaoOcupacao([FromRoute] int id, [FromBody] RestricaoOcupacao restricaoOcupacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != restricaoOcupacao.ID)
            {
                return BadRequest();
            }

            _context.Entry(restricaoOcupacao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RestricaoOcupacaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/RestricaoOcupacao
        [HttpPost]
        public async Task<IActionResult> PostRestricaoOcupacao([FromBody] RestricaoOcupacao restricaoOcupacao)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.RestricoesOcupacao.Add(restricaoOcupacao);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRestricaoOcupacao", new { id = restricaoOcupacao.ID }, restricaoOcupacao);
        }

        // DELETE: api/RestricaoOcupacao/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRestricaoOcupacao([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var restricaoOcupacao = await _context.RestricoesOcupacao.FindAsync(id);
            if (restricaoOcupacao == null)
            {
                return NotFound();
            }

            _context.RestricoesOcupacao.Remove(restricaoOcupacao);
            await _context.SaveChangesAsync();

            return Ok(restricaoOcupacao);
        }

        private bool RestricaoOcupacaoExists(int id)
        {
            return _context.RestricoesOcupacao.Any(e => e.ID == id);
        }
    }
}