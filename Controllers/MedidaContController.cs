using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MedidaContController : ControllerBase
    {
        private readonly SiCContext _context;

        public MedidaContController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/MedidaCont
        [HttpGet]
        public IEnumerable<MedidaCont> GetMedidaCont()
        {
            return _context.MedidasCont;
        }

        // GET: api/MedidaCont/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetMedidaCont([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medidaCont = await _context.MedidasCont.FindAsync(id);

            if (medidaCont == null)
            {
                return NotFound();
            }

            return Ok(medidaCont);
        }

        // PUT: api/MedidaCont/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedidaCont([FromRoute] int id, [FromBody] MedidaCont medidaCont)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != medidaCont.ID)
            {
                return BadRequest();
            }

            _context.Entry(medidaCont).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedidaContExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MedidaCont
        [HttpPost]
        public async Task<IActionResult> PostMedidaCont([FromBody] MedidaCont medidaCont)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.MedidasCont.Add(medidaCont);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMedidaCont", new { id = medidaCont.ID }, medidaCont);
        }

        // DELETE: api/MedidaCont/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMedidaCont([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var medidaCont = await _context.MedidasCont.FindAsync(id);
            if (medidaCont == null)
            {
                return NotFound();
            }

            _context.MedidasCont.Remove(medidaCont);
            await _context.SaveChangesAsync();

            return Ok(medidaCont);
        }

        private bool MedidaContExists(int id)
        {
            return _context.MedidasCont.Any(e => e.ID == id);
        }
    }
}