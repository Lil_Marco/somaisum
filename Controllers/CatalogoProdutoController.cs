﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SiCProject.Models;

namespace SiCProject
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogoProdutoController : ControllerBase
    {
        private readonly SiCContext _context;

        public CatalogoProdutoController(SiCContext context)
        {
            _context = context;
        }

        // GET: api/CatalogoProduto
        [HttpGet]
        public IEnumerable<CatalogoProduto> GetCatalogoProduto()
        {
            return _context.CatalogoProduto;
        }

        // GET: api/CatalogoProduto/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCatalogoProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catalogoProduto = await _context.CatalogoProduto.FindAsync(id);

            if (catalogoProduto == null)
            {
                return NotFound();
            }

            return Ok(catalogoProduto);
        }

        // PUT: api/CatalogoProduto/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCatalogoProduto([FromRoute] int id, [FromBody] CatalogoProduto catalogoProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != catalogoProduto.ID)
            {
                return BadRequest();
            }

            _context.Entry(catalogoProduto).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CatalogoProdutoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CatalogoProduto
        [HttpPost]
        public async Task<IActionResult> PostCatalogoProduto([FromBody] CatalogoProduto catalogoProduto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.CatalogoProduto.Add(catalogoProduto);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCatalogoProduto", new { id = catalogoProduto.ID }, catalogoProduto);
        }

        // DELETE: api/CatalogoProduto/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCatalogoProduto([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var catalogoProduto = await _context.CatalogoProduto.FindAsync(id);
            if (catalogoProduto == null)
            {
                return NotFound();
            }

            _context.CatalogoProduto.Remove(catalogoProduto);
            await _context.SaveChangesAsync();

            return Ok(catalogoProduto);
        }

        private bool CatalogoProdutoExists(int id)
        {
            return _context.CatalogoProduto.Any(e => e.ID == id);
        }
    }
}